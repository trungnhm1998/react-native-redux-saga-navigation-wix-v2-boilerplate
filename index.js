/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { Navigation } from 'react-native-navigation'

import App from './src'
import ScreenConfigs from './src/navigation/screen.config'

Navigation.registerComponent(ScreenConfigs.ENTRY_APP, () => App)

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setDefaultOptions({
    topBar: {
      visible: false,
      drawBehind: true,
      animate: false,
    },
  });
  Navigation.setRoot({
    root: {
      component: {
        name: ScreenConfigs.ENTRY_APP,
      },
    },
  })
})

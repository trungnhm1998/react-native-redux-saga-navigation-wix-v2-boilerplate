import { Navigation } from 'react-native-navigation'
import ScreenConfigs from './screen.config'

const AppNavigation = {
  navToRoot: () => {
    Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component: {
                name: ScreenConfigs.ROOT,
              },
            },
          ],
        },
      },
    })
  },
}

export default AppNavigation

import React from 'react'
import { Navigation } from 'react-native-navigation'
import AppScreens from '../screens' 
import ScreenConfigs from './screen.config'
import StoreProvider from '../configs/store-provider.config'

type Props = {}

const WrappedComponent = (Component) => {
  return (props: Props) => {
    const HOC = () => (
      <StoreProvider>
        <Component {...props} />
      </StoreProvider>
    )

    return <HOC />
  }
}

export default () => {
  Navigation.registerComponent(ScreenConfigs.ROOT, () => WrappedComponent(AppScreens.Root))
  console.info('All screens have been registered...')
} 

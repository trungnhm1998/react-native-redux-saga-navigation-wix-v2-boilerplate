const ENTRY_APP = 'ENTRY_APP'
const ROOT = 'ROOT'

const ScreenConfigs = {
  ENTRY_APP,
  ROOT,
}

export default ScreenConfigs

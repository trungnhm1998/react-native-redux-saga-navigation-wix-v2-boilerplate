import { combineReducers } from 'redux'
import {
  mockupReducer,
} from '../redux'


const rootReducer = combineReducers({
  mockupReducer,
})

export default rootReducer

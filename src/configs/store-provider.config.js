import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { configureStore, AppPersistStore } from './store.config'

let store

class StoreProvider extends PureComponent {
  getChildContext() {
    return {
      store,
    };
  }

  static childContextTypes = {
    store: PropTypes.shape({}),
  };

  render() {
    const { children } = this.props

    store = store || configureStore()

    return (
      <Provider store={store}>
        <PersistGate persistor={AppPersistStore}>
          {children}
        </PersistGate>
      </Provider>
    )
  }
}

export default StoreProvider

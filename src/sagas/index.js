import { fork, all } from 'redux-saga/effects'
import MockupSagas from './mockup';

function* rootSagas() {
  yield all([
    fork(MockupSagas.Mockup),
  ])
}

export default rootSagas

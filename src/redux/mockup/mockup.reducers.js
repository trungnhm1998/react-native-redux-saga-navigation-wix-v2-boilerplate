import MockupTypes from './mockup.types'

const initState = {}

export default function mockupReducer(state = initState, action) {
  switch (action.type) {
  case MockupTypes.MOCK_UP_FAIL:
    return {
      ...state,
      ...action.payload,
    }
  case MockupTypes.MOCK_UP_SUCCESS:
    return {
      ...state,
      ...action.payload,
    }
  default:
    return state
  }
}

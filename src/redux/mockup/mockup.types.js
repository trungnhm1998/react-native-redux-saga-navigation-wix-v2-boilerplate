const MOCK_UP = 'MOCK_UP'
const MOCK_UP_SUCCESS = 'MOCK_UP_SUCCESS'
const MOCK_UP_FAIL = 'MOCK_UP_FAIL'

const MockupTypes = {
  MOCK_UP,
  MOCK_UP_SUCCESS,
  MOCK_UP_FAIL,
}

export default MockupTypes
